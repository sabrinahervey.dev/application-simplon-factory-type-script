# installer l'appli
cloner le projet sur git

# lancer l'appli
* utiliser la commande npm run build (pour compiler)
* ensuite taper la commande node dist (pour exécuter)

# l'application
Ce projet est un système de gestion de commandes conçu pour gérer les commandes des clients, le catalogue de produits, et le processus de livraison. Il permet de créer et de gérer des clients, de gérer un inventaire de produits avec des spécificités telles que les vêtements et les chaussures, et de gérer des commandes



