import { Address } from "../type/address";
// creation de la classe Customer
class Customer {
    //propriétés de la classe pour stocker les données du client
    public customerId: number;
    public name: string;
    public email: string;
     //propriété pour stocker l'adresse du client
    public adress: Address | undefined;
    //constructsize_r pour initialiser les propriétés lors de la creation d'une instance de la classe
    constructor(customerId: number, name: string, email: string) {
        this.customerId = customerId;
        this.name = name;
        this.email = email;
    }
    //methode pour afficher les informations du client
    displayInfo(): string {
        return `CustomerId: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, Address: ${this.adress}`;
    }
    // methode pour renseigner l'adresse du client
    setAddress(address: Address) {
        this.adress = address;
    }
    //methode pour retourner les informations de l'adresse
    displayAddress():string {
        if (this.adress) {
            return `Street: ${this.adress.street}, City: ${this.adress.city}, PostalCode: ${this.adress.postalCode}, Country: ${this.adress.country}`;
        } else {
            return "No address found";
        }
    }
    //methode pour ajouter les informations de l'adresse
    displayInfoWithAddress(): string {
        return `${this.displayInfo()}, ${this.displayAddress()}`;
    }
}


export default Customer;