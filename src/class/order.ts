import customer from './customer';
import product from './product';

class Order {
    calculateWeight(): number {
        //on declare poid que l'on initialise à 0
        let totalWeight = 0;
        //on va calculer le poid total avec le poid produit
        for (const product of this.productsList){
        //somme des poids des produits
        totalWeight += product.weight
        //et on retourne le resultat
    }   return totalWeight;
  }
    public orderId: number;
    public customer: customer;
    productsList: product[];
    orderDate: Date;
  
    constructor(orderId: number, customer: customer) {
      this.orderId = orderId;
      this.customer = customer;
      this.productsList = [];
      this.orderDate = new Date();
    }
  
    addProduct(product: product): void {
      this.productsList.push(product);
    }
    removeProduct(productId: number): void {
        this.productsList = this.productsList.filter(product => product.productId !== productId);
      }
      calculateTotal(): number {
        let totalPrice = 0;
        for (const product of this.productsList) {
          totalPrice += product.price;
        }
        return totalPrice;
      }
      displayOrder(): string {
        return `Order ID: ${this.orderId}, Customer: ${this.customer.name}, Total: ${this.calculateTotal()} €, Date: ${this.orderDate}`;
      }
    } 
    
export default Order;    

