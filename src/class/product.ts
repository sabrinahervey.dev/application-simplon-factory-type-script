import { dimensions } from "../type/dimensions";
import {ClothingSize} from "../type/enum";
import {ShoesSize} from "../type/enum";


 class Product {
        public productId: number;
        public name: string;
        public weight: number;
        public price: number;
        public dimensions: dimensions
        public clothesSize?: ClothingSize;
        public shoesSize?: ShoesSize;

        constructor(productId: number, name: string, weight: number, price: number, dimensions: dimensions, clothesSize?: ClothingSize, shoesSize?: ShoesSize) {
            this.productId = productId;
            this.name = name;
            this.weight = weight;
            this.price = price;
            this.dimensions = dimensions;
            this.clothesSize = clothesSize;
            this.shoesSize = shoesSize;
        }

        displayDetails(): string {
            let details = `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price} €, Dimensions: ${this.dimensions.lenght} x ${this.dimensions.width} x ${this.dimensions.height}`;
            return `${details}, ${this.clothesSize ? `Size: ${this.clothesSize}` : "L"}, ${this.shoesSize ? `Size: ${this.shoesSize}` : "40"}`;
        }
        }

export default Product;    