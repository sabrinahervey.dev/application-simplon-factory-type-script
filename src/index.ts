import Customer from "./class/customer";
import Product from "./class/product";
import Clothing from "./sous-class/clothing";
import { ClothingSize, ShoesSize } from "./type/enum";
import Shoes from "./sous-class/shoes";
import order from "./class/order";


//creation d'une instance de la classe Customer avec les valeurs
const customer1 = new Customer(1, "Homer", "simpson@example.com");
// affichage des informations clients avec la methode displayInfo 
//console.log(customer1.displayInfo());
//console.log(customer1.displayAddress());
console.log(customer1.displayInfoWithAddress());

//----------------------------------------------
    const product1 = new Product(1, "dress", 0.5, 100, { lenght: 50, width: 40, height: 60 });
    console.log(product1.displayDetails());
      
    const shoes2 = new Shoes(2, "Doc Martens noire", 0.7, 150, { lenght: 20, width: 10, height: 15 }, ShoesSize.size_42);
        console.log(shoes2.displayDetails());

        const clothing3 = new Clothing(3, "T-shirt", 0.8, 180, { lenght: 30, width: 20, height: 40 }, ClothingSize.xs);
        console.log(clothing3.displayDetails());

        // Créer une commande
    const orderInstance: order = new order(1, customer1);

    // Ajouter des produits à la commande
    orderInstance.addProduct(product1);
    orderInstance.addProduct(shoes2);

    // Afficher les détails de la commande
    console.log(orderInstance.displayOrder());
    console.log(`Total Weight: ${orderInstance.calculateWeight()} kg`);

    // Supprimer un produit de la commande
    orderInstance.removeProduct(1);

    // Afficher les détails de la commande
    console.log(orderInstance.displayOrder());

    // Afficher le prix total de la commande
    console.log(`Total Price: ${orderInstance.calculateTotal()} €`);

   
    