import Product from "../class/product";
import {dimensions} from "../type/dimensions";
import {ClothingSize} from "../type/enum";

class Clothing extends Product {
    size: ClothingSize;
    constructor(
        productId: number,
        name: string,
        weight: number,
        price: number,
        dimensions: dimensions,
        size: ClothingSize
    ) {
        super(productId, name, weight, price, dimensions);
        this.size = size;
    }

    displayDetails(): string {
        return `${super.displayDetails()}, Size: ${this.size}`;
    }
}   

export default Clothing;



 