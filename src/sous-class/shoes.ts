import Product from "../class/product";
import {dimensions} from "../type/dimensions";
import {ShoesSize} from "../type/enum";

class Shoes extends Product {
    size: ShoesSize;
    constructor(
        productId: number,
        name: string,
        weight: number,
        price: number,
        dimensions: dimensions,
        size: ShoesSize
    ) {
        super(productId, name, weight, price, dimensions); 
        this.size = size;
    }

    displayDetails(): string {
        return `${super.displayDetails()}, Size: ${this.shoesSize}`;
    }
}
export default Shoes;