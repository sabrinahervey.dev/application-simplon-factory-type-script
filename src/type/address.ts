//creation du type Address pour stocker les informations de l'adresse du client
export type Address = {
    street: string;
    city: string;
    postalCode: string;
    country: string;
}