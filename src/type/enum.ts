
export enum ClothingSize {
    xs = 'xs',
    s = 's',
    m = 'm',
    l = 'l',
    xl = 'xl',
    xxl = 'xxl'
}

export enum ShoesSize {
    size_36 = 36,
    size_37 = 37,
    size_38 = 38,
    size_39 = 39,
    size_40 = 40,
    size_41 = 41,
    size_42 = 42,
    size_43 = 43,
    size_44 = 44,
    size_45 = 45
}


